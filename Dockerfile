FROM docker:dind

RUN apk update &&\
    apk add git rsync py3-pip &&\
    pip3 install --upgrade pip &&\
    pip3 install -U git+https://github.com/ansible-community/molecule --break-system-packages &&\
    pip3 install requests pytest-testinfra "molecule[docker]" molecule-docker --break-system-packages &&\
    pip3 install --upgrade molecule==5.1.0 --break-system-packages
